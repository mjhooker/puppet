class proxmox_not_enterprise {

file { '/etc/apt/sources.list.d/pve-enterprise.list':
  ensure => absent,
}
}


Class['apt::update'] -> Package <| title != 'apt-transport-https' and title != 'dirmngr' |>

class debian_apt {
 $module_stdlib = 'puppetlabs-apt'
  exec { 'puppet_module_apt':
    command => "puppet module install ${module_stdlib}",
    unless  => "puppet module list | grep ${module_stdlib}",
    path    => ['/bin','/usr/bin']
  }

}

class proxmox_repro_base {
include proxmox_not_enterprise

class { 'dhcp':
  service_ensure => running,
  dnsdomain      => [
    'dc1.example.net',
    '1.10.in-addr.arpa',
  ],
  nameservers  => ['8.8.8.8'],
  ntpservers   => ['pool.ntp.org'],
  interfaces   => ['vmbr0'],
}

dhcp::pool{ 'ops.dc1.example.net':
  network => '10.1.0.0',
  mask    => '255.255.0.0',
  range   => ['10.1.0.10 10.1.0.100', '10.1.0.200 10.1.0.250' ],
  gateway => '10.1.0.1',
}

augeas{ "vmbr0" :
    context => "/files/etc/network/interfaces",
    changes => [
        "set auto[child::1 = 'vmbr0']/1 vmbr0",
        "set iface[. = 'vmbr0'] vmbr0",
        "set iface[. = 'vmbr0']/family inet",
        "set iface[. = 'vmbr0']/method static",
        "set iface[. = 'vmbr0']/address 10.1.0.1",
        "set iface[. = 'vmbr0']/netmask 255.255.0.0",
        "set iface[. = 'vmbr0']/bridge_ports none",
        "set iface[. = 'vmbr0']/bridge_stp off",
        "set iface[. = 'vmbr0']/bridge_fd 0",
        join(["set iface[. = 'vmbr0']/post-up \"echo 1 > /proc/sys/net/ipv4/ip_forward; iptables -t nat -A POSTROUTING -s '10.1.0.0/16' -o ",mjh::extinterface()," -j MASQUERADE\""],''),
        join(["set iface[. = 'vmbr0']/post-down \"iptables -t nat -D POSTROUTING -s '10.1.0.0/16' -o ",mjh::extinterface()," -j MASQUERADE\""],''),
],
}


file_line { 'etc network interfaces line':
    ensure => present,
    line   => 'source-directory /etc/network/interfaces.d',
    path   => '/etc/network/interfaces',
  }

package { 'proxmox-ve': ensure => 'installed', notify => Reboot['after_run'], }
package { 'ntp': ensure => 'installed', }
package { 'ssh': ensure => 'installed', }
package { 'postfix': ensure => 'installed', }
package { 'ksm-control-daemon': ensure => 'installed', }
package { 'open-iscsi': ensure => 'installed', }
package { 'systemd-sysv': ensure => 'installed', }
package { 'dirmngr': ensure => 'installed', }

reboot { 'after_run':
  apply  => finished,
}


}


class agdu {


exec { "apt-dist-upgrade":
 command => "/usr/bin/apt-get dist-upgrade -y",
 logoutput   => true,
 timeout     => 1800,
}
}

class agu {
include proxmox_not_enterprise
exec { "apt-update":
 command => "/usr/bin/apt-get update",
 logoutput   => true,
 timeout     => 1800,
}

}

class proxmox_repro_stretch {
include proxmox_repro_base

apt::source { 'proxmox-no-sub':
  
  location => 'http://download.proxmox.com/debian/',
  release  => 'stretch',
  repos    => 'pve-no-subscription',
  key      => {
    'id'       => '0x359E95965E2C3D643159CD300D9A1950E2EF0603',
    'content'  => "-----BEGIN PGP PUBLIC KEY BLOCK-----\n\nmQINBFfDyocBEADBqGXU2sVZeyJhjvcYHbkzcjfP9OKBgkPmpKNG8kP+fT+OsX8U\nFCAmIKXMOd/3fWhdSv7V2/3JaiEmsYn1a1vWhIlgFj2VonE/YS9JqqW7suodSon0\nb52XNwxRisOapU40EOUEjSGhoVUuvNNFkXImKEdtIgzVyyFCf2pj+TXBGWhOtCtK\ndu/zctioq85HR3Zk1YokJCho4/uRU7bElmLNFHSmI7jAU33jmU6ZI3MpxTFq0bd5\n+75IQYOQi4SLktE/xFZPaX54DlIzYCaVvjr57/DKOlwa4nnL0PGbfdS9rwBVxN1E\nVvRsLG3z0crtFtunpJxKN1TI4HM/vZzfvTt9FH38Xx1yhwlUZKqx42YCImYJSBY/\nmxx/XjVZqaGSqBoSLgI+zKmOPEoo6i2nhZhCrm/GuuEV+hP5MHch3YhqO2/xYcCP\neeM9CU8ham84m9uCJ6ol8H0iiImztHXHCGWJ1AFq567NOXE407vQNpM2z49bNlR4\nQYvlXuvM0wJLKo+LFTftj6SjyweMdd3FRzxGUDQaG9YjpBe20etBS3ETTySiDnxN\neLVRe2nKG+e36VugaELJ+T8GZlhT+2s34EPrS4WUdqpwsrIouMXPeMPp0z3VO/7A\nqyTlTK5TaDgLj+LQIZF9dI3aXDhH1Z9OKXsS2m7tSBJeBCY15jDFH9Og2wARAQAB\ntElQcm94bW94IFZpcnR1YWwgRW52aXJvbm1lbnQgNS54IFJlbGVhc2UgS2V5IDxw\ncm94bW94LXJlbGVhc2VAcHJveG1veC5jb20+iQI/BBMBCAApBQJXw8qHAhsDBQkS\nzAMABwsJCAcDAgEGFQgCCQoLBBYCAwECHgECF4AACgkQDZoZUOLvBgM6RA/+KKtA\nTciGES4TEgsLuHPoM5E0X4JWhMq2jN6znmzo5kIVmHXEk4LxeeppMoICsc6DMoDL\n9n4M5m5YqlIYAs78SrxjSdDspPeV2/gPegDD/U8rx+OhGNBORpewSi9jq6iq/bWN\nkT2Pwvk/lmDmnHebtCWvxB2y0mkcaAw87w8c5xYgOnnL/slwcegUN7/m6pcien5b\nIjixt75Kq3ol45y4QRnkYDnoejMnlinEB4U2qfdkiVxEpwLZ97ipKo+wIQ9tMqmk\nq8xVoT39+JJESBAaJO3P19NSJiLtNjkPpoNFNOYJRubY9wD9/2Q1jx7V04U/4zuh\nAppsFcGt/cn5K0Vy6KqPgUAyyMjRB/+MKpL/4zdFcpwy6gu7c0eqMdXw1lW9YYF0\nXQhhxVuet1xbVazIH4NgkwCJvOPVcJwILkmGorTtJPvHgS/V+NFYh3n6Q0GWY5gC\n+dturtMnLxsi6GrE0mamqHuJ7mW3zli2zClURCJaePwdd8i+anZzQwT2wg3oXBtA\ngOZgeZFuC4OrGnfK5hj9n/LV1PjMwEmliiFDNRPOGDrmKbn7LBocem3ams0dKxfg\nEh/97QpKJh15NM677TiQmzbFmBBPA5BPLRzPlVi4eemDyv5ggYdSckz+sCiUMzy3\nx7aL/VB66uF1IYQFp5/WeGRMOv3n3Bkq5S2aEO4=\n=hfNW\n-----END PGP PUBLIC KEY BLOCK-----",
  },
  include  => {
    'deb' => true,
  }, notify => Exec['apt_update']
}


}

class proxmox_repro_buster {
include proxmox_repro_base

apt::source { 'proxmox-no-sub':
  
  location => 'http://download.proxmox.com/debian/',
  release  => 'buster',
  repos    => 'pve-no-subscription',
  key      => {
    'id'       => '0x359E95965E2C3D643159CD300D9A1950E2EF0603',
    'content'  => "-----BEGIN PGP PUBLIC KEY BLOCK-----\n\nmQINBFvydv4BEACqs61eF4B+Zz9H0hKJS72SEofK2Gy6a5wZ/Hb4DrGbbfC6fjrO\nb3r4ZrM7G355TD5He7qzcGrxJjgGwH+/w6xRyYliIzxD/lp8UJXcmiZHG+MYYJP6\nq29NWrbEcqPo6onx2tzNytHIUysqUE+mghXtyMN7KUMip7bDAqx2L51CI180Giv1\nwdKUBP2bgKVObyFzK46ZEMzyl2qr9raFnHA8oF1HZRkwwcfSD/dkY7oJvAO1pXgR\n8PzcXnXjoRTCyWlYVZYn54y9OjnB+knN8BlSOLNdBkKZs74XyJ9JlQU9ZfzatXXE\nhMxdDquIAg+g/W9rLpLz5XAGb2GSNvKrU5otjOdUOnD0k1MpFujsSzRWZCIRnywf\nmQ/Lahgo4wYOrQLNGCNdvwMgbwcD9NRjQsPdja94wJNRsmbhFeAKPyF8p3lf9QUH\nY3Vn1iGI6ut7c3uqUv0lKvujroKNc/hFSgcn8bUB+x0OnKE3yEiiGsEyJHGxVhjy\n3FsY/h1SNtM57Wwk9zxjNuqp66jZcTu8foLNh6Ct+mFsor2Y6MxKVJvrcb9rXv54\nYpQAZUjvZK5gnqOWTWrEZkjtNLoGiyuWOU+2RoqTtRA22u9Vlm5C/lduGC7akbVG\nXd8ocDrq4t5IyM3bqF3oru7zGW0hQgsPwbkQcfOawFkQlGEDzf1TrXTafwARAQAB\ntElQcm94bW94IFZpcnR1YWwgRW52aXJvbm1lbnQgNi54IFJlbGVhc2UgS2V5IDxw\ncm94bW94LXJlbGVhc2VAcHJveG1veC5jb20+iQJUBBMBCAA+FiEENTR5+DeB1/jt\nX1rFe/KBLopuiOAFAlvydv4CGwMFCRLMAwAFCwkIBwIGFQgJCgsCBBYCAwECHgEC\nF4AACgkQe/KBLopuiODQZRAAo0kXc090pNskVDr0qB7T2x8UShxvC5E6imZHASq/\nui1wd5Wei+WkPj4ME/1yAvpMrMAq3LbbIgmHbBqzsagQaeL88vWn5c0NtzsrzHoU\n+ql5XrCnbnmXBoCGUgiXA3vq0FaemTzfCBGnHPbsOoPlvHZjXPvpnMOomO39o1xa\nw2Ny8fhhv651CjPpK7DQF5KoMm3LdjXB6nouErJJZDvUaNmGNhHh4HzWiOSLyaE8\nT0UsUR1HqGkzvgE2OuwPjeWFIIRPKeiCFbA+mlEfwb/Lgu6F4D6IsP++ItuG6Q6Y\njAopuK7QXrnFpDfAZmQsbsOgkqqg5dy7xBJATuCPkUk9qMBaeLVqkANq1OlZksPT\nry2399U83i69xsJNW4BBC0JXKWWJpq5d9ZH05OP9wxYR2+K3Hmh4vvkzcgoMEbnF\nrFzpH+eGkWxxZS1AGhMJBXGkmm1eW7ZFQVx6o0w9dWRRqDo7UklVNPImtXuso3nI\nwuYF0+Dv6PeE8EQWLp4FQGHlaEoUmYFug4xiWF1tCcW6UWy6fEhVAcXbbD0IvUjS\n6pL9IKpyOWDJBV0Tya4LmBAzaPB7ljYfEBASvaPVKDcSva6wEM8/vA6Oal2/LVdQ\n8TG5eRrtWxeZxZSQknv0v3IhPujyP9dxvhJfZmVZKQx/oPgEWFmGuQ8ggXtNZL/8\n72I=\n=ssmE\n-----END PGP PUBLIC KEY BLOCK-----",
},
  include  => {
    'deb' => true,
  }, notify => Exec['apt_update']
}


}

class jenkins_mjh {

docker::run { 'jenkins_mjh':
  image            => 'jenkins/jenkins:lts',
  ports            => ['8080', '50000'],
  expose           => ['8080', '50000'],
  net              => 'host',
  volumes          => ['jenkins_home:/var/jenkins_home'],
  volumes_from     => '6446ea52fbc9',
  restart_service  => true,
  privileged       => false,
  pull_on_start    => false,
  extra_parameters => [ '--restart=always' ],
}

}

node "debian-jenkins" {
 class { 'docker':
  version => '17.09.1~ce-0~debian',
 }
 
 include "jenkins_mjh"
}

node "vps491138" {


 package { 'curl': ensure => 'installed', }
 package { 'gnupg2': ensure => 'installed', }
 package { 'software-properties-common': ensure => 'installed', }
 package { 'ca-certificates': ensure => 'installed', }
 package { 'apt-transport-https': ensure => 'installed', }

 class { 'docker':
  version => '17.09.1~ce-0~ubuntu',
 }
}

node "deb-docker" {


 package { 'curl': ensure => 'installed', }
 package { 'gnupg2': ensure => 'installed', }
 package { 'software-properties-common': ensure => 'installed', }
 package { 'ca-certificates': ensure => 'installed', }
 package { 'apt-transport-https': ensure => 'installed', }

 class { 'docker':
  version => '17.09.1~ce-0~debian',
 }
  ::docker::run { 'portainer_mjh':
  image            => 'portainer/portainer',
  ports            => ['9000:9000'],
  net              => 'host',
  volumes          => ['portainer_data:/data','/var/run/docker.sock:/var/run/docker.sock'],
  restart_service  => true,
  privileged       => true,
  pull_on_start    => true,
  extra_parameters => [ '--restart=always' ],
 }
}


node "default" {
include debian_apt
include proxmox_repro_stretch
  
#stage { "agu": before  => Stage["agdu"] }
#stage { "agdu": before  => Stage["main"] }

#class { "agdu":
# stage => agdu,
#}

#class { "agu":
# stage => agu,
#}

file { '/root/example_file.txt':
    ensure => "file",
    owner  => "root",
    group  => "root",
    mode   => "700",
    content => "Congratulations!
Puppet has created this file.
",}


}
 