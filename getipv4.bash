#!/bin/bash
if grep `hostname` /etc/hosts | grep -v 127.0.0.1 ; then
 echo ping ok no need to fix things
else

echo -n "host { '" > setip.pp
tr -d "\n" < /etc/hostname >> setip.pp
echo "':" >> setip.pp
echo -n "ip => " >> setip.pp
dig -4 TXT +short o-o.myaddr.l.google.com @ns1.google.com | tr "\"" "\'" >> setip.pp
echo ",}" >> setip.pp
echo "host { 'localhost': ip => '127.0.0.1' }" >> setip.pp
echo "resources { 'host': purge => true }" >> setip.pp
echo -n > /etc/hosts

fi

