#!/bin/bash

apt-get update

apt-get -y dist-upgrade

apt-get -y install git joe puppet dnsutils

puppet module install puppet-letsencrypt --version 2.2.0

# https://forge.puppet.com/puppet/letsencrypt

puppet module install puppetlabs-apt --version 5.0.1

puppet module install puppetlabs-docker --version 2.0.0

puppet module install puppet-nginx --version 0.10.0

puppet module install puppetlabs-reboot --version 2.0.0

puppet module install puppet-dhcp --version 3.3.0

# docs here https://github.com/puppetlabs/puppetlabs-docker

cd /root

git clone https://bitbucket.org/mjhooker/iso-dl.git

git clone https://bitbucket.org/mjhooker/puppet.git

cd puppet

source ./getipv4.bash

puppet apply setip.pp

puppet apply .
