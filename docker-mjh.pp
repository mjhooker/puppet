node "docker-mjh" {


 package { 'curl': ensure => 'installed', }
 package { 'gnupg2': ensure => 'installed', }
 package { 'software-properties-common': ensure => 'installed', }
 package { 'ca-certificates': ensure => 'installed', }
 package { 'apt-transport-https': ensure => 'installed', }

 class { 'docker':
  version => '17.09.1~ce-0~debian',
 }
  
  ::docker::run { 'jenkins_mjh':
  image            => 'jenkins/jenkins:lts',
  ports            => ['8080:8080', '50000:50000'],
  net              => 'host',
  volumes          => ['jenkins_home_mjh:/var/jenkins_home'],
  restart_service  => true,
  privileged       => false,
  pull_on_start    => true,
  extra_parameters => [ '--restart=always' ],
 }
 
}