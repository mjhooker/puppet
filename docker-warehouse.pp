node "docker-warehouse","docker-warehouse2","docker-warehouse-test","debian-warehouse-test" {


file { '/etc/systemd/system/docker.service.d/http-proxy.conf':
    ensure => "file",
    owner  => "root",
    group  => "root",
    mode   => "644",
    content => "[Service]
Environment=\"HTTP_PROXY=http://192.168.5.5:3128\" \"HTTPS_PROXY=http://192.168.5.5:3128\"
",}

 package { 'curl': ensure => 'installed', }
 package { 'gnupg2': ensure => 'installed', }
 package { 'software-properties-common': ensure => 'installed', }
 package { 'ca-certificates': ensure => 'installed', }
 package { 'apt-transport-https': ensure => 'installed', }

 class { 'docker':
#  version => '18.06.1~ce-0~debian',
 }
  
#  ::docker::run { 'jenkins_mjh':
#  image            => 'jenkins/jenkins:lts',
#  ports            => ['8080:8080', '50000:50000'],
#  net              => 'host',
#  volumes          => ['jenkins_home_mjh:/var/jenkins_home'],
#  restart_service  => true,
#  privileged       => false,
#  pull_on_start    => true,
#  extra_parameters => [ '--restart=always' ],
# }
 
}