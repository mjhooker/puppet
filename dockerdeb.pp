
node "dockerdeb" {
 package { 'curl': ensure => 'installed', }
 package { 'gnupg2': ensure => 'installed', }
 package { 'software-properties-common': ensure => 'installed', }
 package { 'ca-certificates': ensure => 'installed', }
 package { 'apt-transport-https': ensure => 'installed', }

 class { 'docker':
  version => '17.09.1~ce-0~debian',
 }
  ::docker::run { 'portainer_mjh':
  image            => 'portainer/portainer',
  ports            => ['9000:9000'],
  net              => 'host',
  volumes          => ['portainer_data:/data','/var/run/docker.sock:/var/run/docker.sock'],
  restart_service  => true,
  privileged       => true,
  pull_on_start    => true,
  extra_parameters => [ '--restart=always' ],
 }
}
