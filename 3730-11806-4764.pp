node "3730-11806-4764" {

ssh_authorized_key { 'k':
  ensure => present,
  user   => 'root',
  type   => 'ssh-rsa',
  key    => 'AAAAB3NzaC1yc2EAAAABIwAAAQEAsQ+GhZjoQpNH7aI/8KIv3vyO/lPCA5FPYDuThY8vJWEWtNnZPJIO10Xo20ipAxRWXqB90ftw4HzGodGr1YRCoZYfLGt5J8MVuGgyz5TErKqEDzUfQm+l3b01ucahMgOgWMMkBxxAG4/fImXzjv1lnLaR3ZfXa72iJDftwEKP4NicM0AP66vG3Ist0SkvriGW3RzpBZ3AXzJH2mzRsaOLRilDKjR7yBvrIAqL7TqWcqrOX02I4i0EHCxNdLByiPWXCDKgCn8ZtvGSaKzxYwCPMaYSI+LqF4TubstKjbKMJURiIB8WXjyJ81SAfLmEBbHOSQk6Xwouhz+0CWiYSdkjtQ==',
}

ssh_authorized_key { 'c':
  ensure => present,
  user   => 'root',
  type   => 'ssh-rsa',
  key    => 'AAAAB3NzaC1yc2EAAAABIwAAAQEA3HSaG8APZmk4CrIiB4PJLoVlnRoeNAsmqF4oxR9t0xn/3//fZjeJ4/tzPBTRo86tTrQoKxw4oUxQSzkeuxlqf6jfsv+ZY8Ti6JiGh5X3E6jn1MMrwidlLzTE4tjDslvXHFy/Tg3SHmBhqzfLVIxUQUevyeP4GebH3qq5eNTP1N96zef27/teyK1jqLHVnZTLM+0+v6UmOtXE8NQrvJS4FUbscw+2Pmlt74NYQ5sJ7mCzA/XDXuI542KNVe5EJcHKOAmNacmHmIFkwECGG6LbSUCZyU8qnsmzhFtmDrvXTrEUd9oD/R8WJe9aYQW+FHR/SMVwbFoUlsy+69bShdE87Q==',
}


user { 'root':
  ensure         => present,
  purge_ssh_keys => true,
  home => '/root',
}

class { ::letsencrypt:
  email => 'mjhooker@gmail.com',
}

 package { 'curl': ensure => 'installed', }
 package { 'gnupg2': ensure => 'installed', }
 package { 'software-properties-common': ensure => 'installed', }
 package { 'ca-certificates': ensure => 'installed', }
 package { 'apt-transport-https': ensure => 'installed', }
 #package { 'apache2': ensure => 'installed', }

 letsencrypt::certonly { 'domains':
   domains => ['vps2.mjhooker.me.uk', 'rancher.mjhooker.me.uk', 'kubernetes.mjhooker.me.uk'],
 #  plugin  => 'apache',
 }

class { 'nginx': }

nginx::resource::server { 'rancher.mjhooker.me.uk':
  listen_port => 443,
  proxy       => 'https://localhost:8080',
  ssl                  => true,
  ssl_cert             => '/etc/letsencrypt/archive/vps2.mjhooker.me.uk-0001/fullchain3.pem',
  ssl_key              => '/etc/letsencrypt/archive/vps2.mjhooker.me.uk-0001/privkey3.pem',
  ssl_port             => 443,
}

nginx::resource::server { 'kubernetes.mjhooker.me.uk':
  listen_port => 443,
  proxy       => 'https://10.42.146.172:6443',
  ssl                  => true,
  ssl_cert             => '/etc/letsencrypt/archive/vps2.mjhooker.me.uk-0001/fullchain3.pem',
  ssl_key              => '/etc/letsencrypt/archive/vps2.mjhooker.me.uk-0001/privkey3.pem',
  ssl_port             => 443,
}

 class { 'docker':
  version => '18.06.1~ce~3-0~debian',
 }
  ::docker::run { 'portainer-ssl':
  image            => 'portainer/portainer',
  command          => '--ssl --sslcert /certs/cert3.pem --sslkey /certs/privkey3.pem',
  ports            => ['9000:9000'],
  net              => 'host',
  volumes          => ['portainer_data:/data', '/var/run/docker.sock:/var/run/docker.sock', '/etc/letsencrypt/archive/vps2.mjhooker.me.uk-0001:/certs'],
  restart_service  => true,
  privileged       => true,
  pull_on_start    => true,
  extra_parameters => [ '--restart=always', ],
 }
  ::docker::run { 'rancher':
  image            => 'rancher/server:stable',
  ports            => ['8080:8080'],
  net              => 'host',
  restart_service  => true,
  privileged       => false,
  pull_on_start    => true,
  extra_parameters => [ '--restart=unless-stopped', ],
 }

}
